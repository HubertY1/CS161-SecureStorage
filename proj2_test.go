package proj2

// You MUST NOT change what you import.  If you add ANY additional
// imports it will break the autograder, and we will be Very Upset.

import (
	_ "encoding/hex"
	_ "encoding/json"
	_ "errors"
	"reflect"
	_ "strconv"
	_ "strings"
	"testing"

	"github.com/cs161-staff/userlib"
	_ "github.com/google/uuid"
)

func clear() {
	// Wipes the storage so one test does not affect another
	userlib.DatastoreClear()
	userlib.KeystoreClear()
}

func TestInitUser(t *testing.T) {
	clear()
	t.Log("InitUser test")
	userlib.SetDebugStatus(true)

	_, err := InitUser("alice", "fubar")
	if err != nil {
		t.Error("InitUser failed", err)
		return
	}
	_, err = InitUser("alice", "fubar")
	if err == nil {
		t.Error("InitUser worked on duplicate username", err)
		return
	}
	_, err = InitUser("", "fubar")
	if err == nil {
		t.Error("InitUser worked on empty username", err)
		return
	}
}

func TestGetUser(t *testing.T) {
	clear()
	t.Log("GetUser test")
	userlib.SetDebugStatus(true)

	alice, _ := InitUser("alice", "fubar")
	loadalice, err := GetUser("alice", "fubar")
	if err != nil {
		t.Error("GetUser failed", err)
		return
	}
	loadalice2, err := GetUser("alice", "fubar")
	if err != nil {
		t.Error("Second GetUser failed", err)
		return
	}
	if !reflect.DeepEqual(alice, loadalice) {
		t.Error("User struct not the same", err)
		return
	}
	if !reflect.DeepEqual(alice, loadalice2) {
		t.Error("User struct not the same", err)
		return
	}
	_, err = GetUser("", "fubar")
	if err == nil {
		t.Error("GetUser worked on empty username", err)
		return
	}
	_, err = GetUser("nonexistent", "fubar")
	if err == nil {
		t.Error("GetUser worked on nonexistent username", err)
		return
	}
	_, err = GetUser("alice", "")
	if err == nil {
		t.Error("GetUser worked on empty password", err)
		return
	}
	_, err = GetUser("alice", "wrong")
	if err == nil {
		t.Error("GetUser worked on incorrect password", err)
		return
	}
	_, err = GetUser("alicef", "ubar")
	if err == nil {
		t.Error("GetUser worked on shifted username/password", err)
		return
	}
}

func TestStoreLoadFile(t *testing.T) {
	clear()
	alice, _ := InitUser("alice", "fubar")
	data := []byte("hello world")
	moredata := []byte("goodbye moon")
	empty := []byte("")

	alice.StoreFile("doc1", data)
	alice.StoreFile("doc2", moredata)
	alice.StoreFile("empty", []byte{})

	doc1, err := alice.LoadFile("doc1")
	if err != nil {
		t.Error("LoadFile failed", err)
		return
	}
	doc2, err := alice.LoadFile("doc2")
	if err != nil {
		t.Error("LoadFile failed", err)
		return
	}
	doc3, err := alice.LoadFile("empty")
	if err != nil {
		t.Error("LoadFile failed", err)
		return
	}
	if !reflect.DeepEqual(data, doc1) {
		t.Error("File contents not equal", err)
		return
	}
	if !reflect.DeepEqual(moredata, doc2) {
		t.Error("File contents not equal", err)
		return
	}
	if len(doc3) != 0 || len(empty) != 0 {
		t.Error("Storing empty things didnt work", err)
		return
	}
	_, err = alice.LoadFile("nonexistent")
	if err == nil {
		t.Error("LoadFile worked on nonexistent file", err)
		return
	}

	alice.StoreFile("doc2", data)
	doc2, err = alice.LoadFile("doc2")
	if err != nil {
		t.Error("LoadFile on overwritten failed", err)
		return
	}
	if !reflect.DeepEqual(data, doc2) {
		t.Error("File overwrite failed", err)
		return
	}
}

func TestAppendFile(t *testing.T) {
	clear()
	alice, _ := InitUser("alice", "fubar")
	data := []byte("hello world")
	moredata := []byte("goodbye moon")
	concat := []byte("hello worldgoodbye moon")

	alice.StoreFile("doc1", data)
	err := alice.AppendFile("doc1", moredata)
	if err != nil {
		t.Error("AppendFile failed", err)
		return
	}
	doc, err := alice.LoadFile("doc1")
	if err != nil {
		t.Error("LoadFile on concatenated file failed", err)
		return
	}
	if !reflect.DeepEqual(concat, doc) {
		t.Error("Appended file contents incorrect", err)
		return
	}
	err = alice.AppendFile("nonexistent", moredata)
	if err == nil {
		t.Error("AppendFile worked on nonexistent file", err)
		return
	}
	err = alice.AppendFile("doc1", []byte{})
	if err != nil {
		t.Error("AppendFile with empty byte array failed", err)
		return
	}
	doc, err = alice.LoadFile("doc1")
	if err != nil {
		t.Error("LoadFile on concatenated file failed", err)
		return
	}
	if !reflect.DeepEqual(concat, doc) {
		t.Error("Appended file contents with empty byte array incorrect", err)
		return
	}
	err = alice.AppendFile("doc1", moredata)
	if err != nil {
		t.Error("AppendFile failed", err)
		return
	}
	err = alice.AppendFile("doc1", moredata)
	if err != nil {
		t.Error("AppendFile failed", err)
		return
	}
	err = alice.AppendFile("doc1", moredata)
	if err != nil {
		t.Error("AppendFile failed", err)
		return
	}
	err = alice.AppendFile("doc1", moredata)
	if err != nil {
		t.Error("AppendFile failed", err)
		return
	}
	err = alice.AppendFile("doc1", moredata)
	if err != nil {
		t.Error("AppendFile failed", err)
		return
	}
}

func TestShareReceiveFile(t *testing.T) {
	clear()
	alice, _ := InitUser("alice", "fubar")
	bob, _ := InitUser("bob", "fubar")
	carol, _ := InitUser("carol", "fubar")

	data := []byte("hello world")
	moredata := []byte("goodbye moon")

	alice.StoreFile("doc1", data)
	alice.StoreFile("doc2", moredata)
	magic, err := alice.ShareFile("doc1", "bob")
	if err != nil {
		t.Error("ShareFile failed", err)
		return
	}
	magic2, err := alice.ShareFile("doc1", "bob")
	if err != nil {
		t.Error("ShareFile failed", err)
		return
	}
	_, err = alice.ShareFile("nonexistent", "bob")
	if err == nil {
		t.Error("ShareFile worked on nonexistent file", err)
		return
	}
	_, err = alice.ShareFile("doc1", "nonexistent")
	if err == nil {
		t.Error("ShareFile worked on nonexistent user", err)
		return
	}
	err = bob.ReceiveFile("bobdoc1", "alice", magic)
	if err != nil {
		t.Error("ReceiveFile failed", err)
		return
	}
	err = bob.ReceiveFile("bobdoc1", "alice", magic2)
	if err == nil {
		t.Error("ReceiveFile worked on duplicate filename", err)
		return
	}
	err = bob.ReceiveFile("bobdoc2", "nonexistent", magic2)
	if err == nil {
		t.Error("ReceiveFile worked with nonexistent sender", err)
		return
	}
	err = bob.ReceiveFile("bobdoc2", "carol", magic2)
	if err == nil {
		t.Error("ReceiveFile worked with wrong sender", err)
		return
	}
	err = bob.ReceiveFile("bobdoc2", "alice", "badmagic")
	if err == nil {
		t.Error("ReceiveFile worked with nonsense magic string", err)
		return
	}
	err = carol.ReceiveFile("bobdoc2", "alice", magic2)
	if err == nil {
		t.Error("ReceiveFile worked with wrong recipient", err)
		return
	}
	err = bob.ReceiveFile("bobdoc2", "alice", magic2)
	if err != nil {
		t.Error("ReceiveFile failed", err)
		return
	}

	bobdata, err := bob.LoadFile("bobdoc1")
	if err != nil {
		t.Error("LoadFile failed with shared file", err)
		return
	}
	if !reflect.DeepEqual(data, bobdata) {
		t.Error("Shared content not the same", err)
		return
	}
	alice.StoreFile("doc1", moredata)
	bobdata, err = bob.LoadFile("bobdoc1")
	if err != nil {
		t.Error("LoadFile failed with shared file", err)
		return
	}
	if !reflect.DeepEqual(moredata, bobdata) {
		t.Error("Shared content not the same after edit by owner", err)
		return
	}
	bob.StoreFile("bobdoc1", data)
	alicedata, err := alice.LoadFile("doc1")
	if err != nil {
		t.Error("LoadFile failed with shared file", err)
		return
	}
	if !reflect.DeepEqual(data, alicedata) {
		t.Error("Shared content not the same after edit by sharee", err)
		return
	}
	bobmagic, err := bob.ShareFile("bobdoc1", "carol")
	if err != nil {
		t.Error("ShareFile failed on third level", err)
		return
	}
	bob.StoreFile("bobdoc1", moredata)
	err = carol.ReceiveFile("caroldoc1", "bob", bobmagic)
	if err != nil {
		t.Error("ReceiveFile failed on third level", err)
		return
	}
	caroldata, err := carol.LoadFile("caroldoc1")
	if err != nil {
		t.Error("LoadFile failed with shared file", err)
		return
	}
	if !reflect.DeepEqual(moredata, caroldata) {
		t.Error("Shared content not the same after edit by sharer", err)
		return
	}
	carol.StoreFile("caroldoc1", data)
	bobdata, err = bob.LoadFile("bobdoc1")
	if err != nil {
		t.Error("LoadFile failed with shared file", err)
		return
	}
	if !reflect.DeepEqual(data, bobdata) {
		t.Error("Shared content not the same after edit by sharee on thirdlevel", err)
		return
	}
	err = alice.AppendFile("doc1", data)
	if err != nil {
		t.Error("owner appendfile failed", err)
	}
	err = bob.AppendFile("bobdoc1", data)
	if err != nil {
		t.Error("sharee appendfile failed", err)
	}
	err = carol.AppendFile("caroldoc1", data)
	if err != nil {
		t.Error("third-level appendfile failed", err)
	}
}

func TestRevokeFile(t *testing.T) {
	clear()
	omastar, _ := InitUser("omastar", "fubar")
	alice, _ := InitUser("alice", "fubar")
	bob, _ := InitUser("bob", "fubar")
	carol, _ := InitUser("carol", "fubar")
	daryl, _ := InitUser("daryl", "fubar")
	InitUser("eadric", "fubar")

	data := []byte("hello world")

	omastar.StoreFile("file", data)

	_, err := omastar.LoadFile("file")
	if err != nil {
		t.Error("omastar cant see the file", err)
		return
	}

	oashare, _ := omastar.ShareFile("file", "alice")
	alice.ReceiveFile("file", "omastar", oashare)
	abshare, _ := alice.ShareFile("file", "bob")
	bob.ReceiveFile("file", "alice", abshare)
	bcshare, _ := bob.ShareFile("file", "carol")
	carol.ReceiveFile("file", "bob", bcshare)
	adshare, _ := alice.ShareFile("file", "daryl")
	daryl.ReceiveFile("file", "alice", adshare)

	err = alice.RevokeFile("file", "bob")
	if err != nil {
		t.Error("Revokefile failed", err)
		return
	}

	_, err = omastar.LoadFile("file")
	if err != nil {
		t.Error("omastar cant see the file", err)
		return
	}
	_, err = alice.LoadFile("file")
	if err != nil {
		t.Error("alice cant see the file", err)
		return
	}
	_, err = bob.LoadFile("file")
	if err == nil {
		t.Error("bob can see the file", err)
		return
	}
	_, err = carol.LoadFile("file")
	if err == nil {
		t.Error("carol can see the file", err)
		return
	}
	_, err = daryl.LoadFile("file")
	if err != nil {
		t.Error("daryl cant see the file", err)
		return
	}
	err = bob.AppendFile("file", data)
	if err == nil {
		t.Error("bob can append to the file", err)
		return
	}
	_, err = bob.ShareFile("file", "eadric")
	if err == nil {
		t.Error("bob can share the file", err)
		return
	}
	err = alice.RevokeFile("file", "nonexistent")
	if err == nil {
		t.Error("RevokeFile worked on nonexistent user", err)
		return
	}
	err = alice.RevokeFile("file", "bob")
	if err == nil {
		t.Error("RevokeFile worked on revoked user", err)
		return
	}
	err = alice.RevokeFile("file", "carol")
	if err == nil {
		t.Error("RevokeFile worked on non-direct child", err)
		return
	}
}

func TestSimulOps(t *testing.T) {
	InitUser("alice", "fubar")
	InitUser("bob", "fubar")
	alice1, err := GetUser("alice", "fubar")
	if err != nil {
		t.Error("GetUser failed", err)
		return
	}
	alice2, err := GetUser("alice", "fubar")
	if err != nil {
		t.Error("GetUser failed", err)
		return
	}
	data := []byte("hello world")
	alice1.StoreFile("file", data)
	_, err = alice2.LoadFile("file")
	if err != nil {
		t.Error("second-instance LoadFile failed", err)
		return
	}
	err = alice2.AppendFile("file", data)
	if err != nil {
		t.Error("second-instance AppendFile failed", err)
		return
	}
	err = alice2.AppendFile("file", data)
	if err != nil {
		t.Error("second-instance AppendFile failed", err)
		return
	}
	_, err = alice2.ShareFile("file", "bob")
	if err != nil {
		t.Error("second-instance ShareFile failed", err)
		return
	}
	err = alice1.RevokeFile("file", "bob")
	if err != nil {
		t.Error("first-instance RevokeFile failed", err)
		return
	}

}
