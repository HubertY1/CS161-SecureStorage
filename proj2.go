package proj2

// CS 161 Project 2 Fall 2020
// You MUST NOT change what you import.  If you add ANY additional
// imports it will break the autograder. We will be very upset.

import (
	"github.com/cs161-staff/userlib"

	// Life is much easier with json:  You are
	// going to want to use this so you can easily
	// turn complex structures into strings etc...
	"encoding/json"

	// Likewise useful for debugging, etc...
	"encoding/hex"

	// UUIDs are generated right based on the cryptographic PRNG
	// so lets make life easier and use those too...
	//
	// You need to add with "go get github.com/google/uuid"

	// Useful for debug messages, or string manipulation for datastore keys.

	// Want to import errors.
	"errors"

	// Optional. You can remove the "_" there, but please do not touch
	// anything else within the import bracket.
	_ "strconv"
	// if you are looking for fmt, we don't give you fmt, but you can use userlib.DebugMsg.
	// see someUsefulThings() below:
)

func printBytes(b []byte) {
	h := hex.EncodeToString(b)
	userlib.DebugMsg("The hex: %v", h)
}

type DocAddr struct {
	Addr   userlib.UUID
	Author string
}

func (a *DocAddr) equals(b DocAddr) bool {
	return a.Addr == b.Addr && a.Author == b.Author
}

type SecureDocument struct {
	Meta    []byte
	MetaSig []byte
	Body    []byte
	BodySig []byte
}

type EncryptedDocument struct {
	Body     []byte
	BodyHMAC []byte
}

type FileHeadBody struct {
	Next  userlib.UUID
	Last  userlib.UUID
	Pages int
}

type FileNodeBody struct {
	Next userlib.UUID
	Data []byte
}

func fetchPublicKeys(user string) (ek userlib.PKEEncKey, vk userlib.DSVerifyKey, ok bool) {
	ek, ok1 := userlib.KeystoreGet("e " + user)
	vk, ok2 := userlib.KeystoreGet("v " + user)
	return ek, vk, ok1 && ok2
}

func setPublicKeys(user string) (ek userlib.PKEEncKey, dk userlib.PKEDecKey, vk userlib.DSVerifyKey, sk userlib.DSSignKey) {
	ek, dk, err := userlib.PKEKeyGen()
	if err != nil {
		panic("PKEGen failed unexpectedly!!!")
	}
	sk, vk, err = userlib.DSKeyGen()
	if err != nil {
		panic("DSKeyGen failed unexpectedly!!!")
	}
	err = userlib.KeystoreSet("e "+user, ek)
	if err != nil {
		panic("KeystoreSet failed unexpectedly!!!")
	}
	err = userlib.KeystoreSet("v "+user, vk)
	if err != nil {
		panic("KeystoreSet failed unexpectedly!!!")
	}
	return ek, dk, vk, sk
}

func symEnc(key []byte, plaintext []byte) []byte {
	size := len(plaintext)
	paddedsize := (size/userlib.AESBlockSize + 1) * userlib.AESBlockSize
	pad := paddedsize - size
	for i := 0; i < pad; i++ {
		plaintext = append(plaintext, byte(pad))
	}
	return userlib.SymEnc(key, userlib.RandomBytes(userlib.AESBlockSize), plaintext)
}

func symDec(key []byte, ciphertext []byte) []byte {
	plaintext := userlib.SymDec(key, ciphertext)
	pad := int(plaintext[len(plaintext)-1])
	return plaintext[0 : len(plaintext)-pad]
}

func pkEnc(ek userlib.PKEEncKey, plaintext []byte, flag string) []byte {
	if len(plaintext) == 0 {
		return []byte{}
	}
	ret, err := userlib.PKEEnc(ek, plaintext)
	if err != nil {
		panic(flag)
		//return []byte{}
	}
	return ret
}

func pkDec(dk userlib.PKEDecKey, ciphertext []byte) ([]byte, error) {
	if len(ciphertext) == 0 {
		return []byte{}, nil
	}
	return userlib.PKEDec(dk, ciphertext)
}

func DSSign(sk userlib.DSSignKey, data []byte) []byte {
	ret, err := userlib.DSSign(sk, data)
	if err != nil {
		panic("DSSign failed unexpectedly!!!")
	}
	return ret
}

func hashKDF(key []byte, purpose string) []byte {
	ret, err := userlib.HashKDF(key, []byte(purpose))
	if err != nil {
		panic("HashKDF failed unexpectedly!!!")
	}
	return ret[:16]
}

func hashKDFnum(key []byte, num int) []byte {
	ret, err := userlib.HashKDF(key, []byte{byte(num)})
	if err != nil {
		panic("HashKDF failed unexpectedly!!!")
	}
	return ret[:16]
}

func randomUUID() (ret userlib.UUID) {
	data := userlib.RandomBytes(16)
	for x := range ret {
		ret[x] = data[x]
	}
	return
}

func hashToUUID(data [64]byte) (ret userlib.UUID) {
	for x := range ret {
		ret[x] = data[x]
	}
	return
}

func jsonMarshal(v interface{}) []byte {
	ret, err := json.Marshal(v)
	if err != nil {
		panic("json.Marshal failed unexpectedly!!!")
	}
	return ret
}

func jsonUnmarshal(data []byte, v interface{}) (err error) {
	for len(data) > 0 && data[len(data)-1] == byte(0) {
		data = data[:len(data)-1]
	}
	return json.Unmarshal(data, v)
}

func (d *SecureDocument) verifyMeta(vk userlib.DSVerifyKey) error {
	return userlib.DSVerify(vk, d.Meta, d.MetaSig)
}
func (d *SecureDocument) verifyBody(vk userlib.DSVerifyKey) error {
	return userlib.DSVerify(vk, d.Body, d.BodySig)
}

func (d *EncryptedDocument) verify(symkey []byte) error {
	hmac, err := userlib.HMACEval(symkey, d.Body)
	if err != nil {
		return err
	}
	if !userlib.HMACEqual(hmac, d.BodyHMAC) {
		return errors.New("hmac verify failed")
	}
	return nil
}

func (d *SecureDocument) signMeta(sk userlib.DSSignKey) {
	d.MetaSig = DSSign(sk, d.Meta)
}

func (d *SecureDocument) signBody(sk userlib.DSSignKey) {
	d.BodySig = DSSign(sk, d.Body)
}

func (d *SecureDocument) write(key userlib.UUID) {
	data := jsonMarshal(d)
	userlib.DatastoreSet(key, data)
}

func (d *EncryptedDocument) write(key userlib.UUID, symkey []byte) {
	enck := hashKDF(symkey, "encrypt")
	hmack := hashKDF(symkey, "hmac")

	var err error
	d.Body = symEnc(enck, d.Body)
	d.BodyHMAC, err = userlib.HMACEval(hmack, d.Body)
	if err != nil {
		panic("HMACEval failed unexpectedly!!!")
	}
	data := jsonMarshal(d)
	userlib.DatastoreSet(key, data)
}

func (d *SecureDocument) read(key userlib.UUID) (err error) {
	data, ok := userlib.DatastoreGet(key)
	if !ok || data == nil {
		return errors.New("datastore entry missing")
	}
	if err := jsonUnmarshal(data, &d); err != nil {
		return err
	}
	return nil
}

func (d *EncryptedDocument) read(key userlib.UUID, symkey []byte) (err error) {
	deck := hashKDF(symkey, "encrypt")
	hmack := hashKDF(symkey, "hmac")
	data, ok := userlib.DatastoreGet(key)

	if !ok || data == nil {
		return errors.New("datastore entry missing")
	}
	if err := jsonUnmarshal(data, &d); err != nil {
		return err
	}
	if err := d.verify(hmack); err != nil {
		return err
	}
	d.Body = symDec(deck, d.Body)
	return nil
}

type FileHandle struct {
	FilenameHash [64]byte
	ShareNode    DocAddr
}

// User is the structure definition for a user record
type User struct {
	Username    string
	MasterKey   []byte
	PKEEncKey   userlib.PKEEncKey
	PKEDecKey   userlib.PKEDecKey
	DSVerifyKey userlib.DSVerifyKey
	DSSignKey   userlib.DSSignKey
	Files       []FileHandle
}

func (userdata *User) read(username string, password string) (err error) {
	masterkey := userlib.Argon2Key([]byte(password), []byte(username), 16)
	addr := hashToUUID(userlib.Hash([]byte(username)))

	var doc EncryptedDocument
	if err := doc.read(addr, masterkey); err != nil {
		return errors.New("INVALID CREDENTIALS or INTEGRITY COMPROMISED (hmac failed)")
	}
	if err := jsonUnmarshal(doc.Body, &userdata); err != nil {
		return errors.New("user data could not be unmarshaled")
	}
	return nil
}

func (userdata *User) update() error {
	addr := hashToUUID(userlib.Hash([]byte(userdata.Username)))
	var doc EncryptedDocument
	if err := doc.read(addr, userdata.MasterKey); err != nil {
		return errors.New("INVALID CREDENTIALS or INTEGRITY COMPROMISED (hmac failed)")
	}
	if err := jsonUnmarshal(doc.Body, &userdata); err != nil {
		return errors.New("user data could not be unmarshaled")
	}
	return nil
}

func (userdata *User) write() {
	addr := hashToUUID(userlib.Hash([]byte(userdata.Username)))
	var userdoc EncryptedDocument
	userdoc.Body = jsonMarshal(userdata)
	userdoc.write(addr, userdata.MasterKey)
}

type ShareEntry struct {
	PubKey userlib.PKEEncKey
	Value  []byte
}

type ShareNodeBody struct {
	PubKey           userlib.PKEEncKey
	EncryptedPrivKey []byte
	LastModified     DocAddr
	Data             []ShareEntry
}

type ShareNodeMeta struct {
	DocAddr  DocAddr
	Parent   DocAddr
	Children []DocAddr
	Depth    int
}

type ShareNode struct {
	Meta ShareNodeMeta
	Body ShareNodeBody
	Doc  SecureDocument
}

func (s *ShareNode) read(addr DocAddr) (err error) {
	if err = s.Doc.read(addr.Addr); err != nil {
		return
	}
	if err = jsonUnmarshal(s.Doc.Meta, &s.Meta); err != nil {
		return
	}
	if err = jsonUnmarshal(s.Doc.Body, &s.Body); err != nil {
		return
	}
	if !addr.equals(s.Meta.DocAddr) {
		return errors.New("INTEGRITY COMPROMISED (inconsistent meta)")
	}
	return nil
}

func (s *ShareNode) readAndVerifyParent(child ShareNode) (err error) {
	if child.Meta.Depth == 0 {
		return errors.New("depth is already 0")
	}
	if err = s.read(child.Meta.Parent); err != nil {
		return err
	}
	if s.Meta.Depth != child.Meta.Depth-1 {
		return errors.New("unexpected depth")
	}
	for _, c := range s.Meta.Children {
		if c.equals(child.Meta.DocAddr) {
			return nil
		}
	}
	return errors.New("delisted by parent")
}

func (s ShareNode) isAncestor(ancestor ShareNode) bool {
	for s.Meta.Depth < ancestor.Meta.Depth {
		if err := s.readAndVerifyParent(s); err != nil {
			return false
		}
	}
	return s.Meta.DocAddr.equals(ancestor.Meta.DocAddr)
}

func (s *ShareNode) getSymKey(ek userlib.PKEEncKey, dk userlib.PKEDecKey) (ret []byte, err error) {
	for _, c := range s.Body.Data {
		if keyqual(ek, c.PubKey) {
			ret, err = pkDec(dk, c.Value)
			if len(ret) != 16 {
				err = errors.New("bad decrypted key length")
			}
			return
		}
	}
	err = errors.New("public key not found in sharenode")
	return
}

func (s *ShareNode) getPrivKey(ek userlib.PKEEncKey, dk userlib.PKEDecKey) (ret userlib.PKEDecKey, err error) {
	symkey, er := s.getSymKey(ek, dk)
	if er != nil {
		err = er
		return
	}
	data := symDec(symkey, s.Body.EncryptedPrivKey)
	err = jsonUnmarshal(data, &ret)
	return
}

//Public key exposed by a sharenode can only be trusted after running this.
func (s *ShareNode) verifySignatures(defaultKey userlib.DSVerifyKey) (err error) {
	_, vk, ok := fetchPublicKeys(s.Meta.DocAddr.Author)
	if !ok {
		return errors.New("INTEGRITY COMPROMISED (user not found)")
	}
	if err = s.Doc.verifyMeta(vk); err != nil {
		//also trust own signature
		if err = s.Doc.verifyMeta(defaultKey); err != nil {
			return errors.New("INTEGRITY COMPROMISED (could not verify metadata)")
		}
	}
	if s.Body.LastModified.equals(s.Meta.DocAddr) {
		if err = s.Doc.verifyBody(vk); err != nil {
			return errors.New("INTEGRITY COMPROMISED (could not verify body)")
		}
	}
	var signer ShareNode
	if err = signer.read(s.Body.LastModified); err != nil {
		return errors.New("INTEGRITY COMPROMISED (signer unreadable)")
	}
	if s.isAncestor(signer) {
		_, vk, ok := fetchPublicKeys(signer.Meta.DocAddr.Author)
		if !ok {
			return errors.New("INTEGRITY COMPROMISED (user not found)")
		}
		if err = s.Doc.verifyBody(vk); err != nil {
			return errors.New("INTEGRITY COMPROMISED (could not verify body)")
		}
	}
	return nil
}

// InitUser creates a user.  It will only be called once for a user
// (unless the keystore and datastore are cleared during testing purposes)
// It should store a copy of the userdata, suitably encrypted, in the
// datastore and should store the user's public key in the keystore.
// The datastore may corrupt or completely erase the stored
// information, but nobody outside should be able to get at the stored
// You are not allowed to use any global storage other than the
// keystore and the datastore functions in the userlib library.
// You can assume the password has strong entropy, EXCEPT
// the attackers may possess a precomputed tables containing
// hashes of common passwords downloaded from the internet.
func InitUser(username string, password string) (userdataptr *User, err error) {
	if len(username) == 0 {
		return nil, errors.New("USERNAME EMPTY")
	}
	_, _, ok := fetchPublicKeys(username)
	if ok {
		return nil, errors.New("USERNAME IN USE")
	}

	var userdata User
	userdata.Username = username
	userdata.MasterKey = userlib.Argon2Key([]byte(password), []byte(username), 16)
	userdata.PKEEncKey, userdata.PKEDecKey, userdata.DSVerifyKey, userdata.DSSignKey = setPublicKeys(username)
	userdata.write()

	return &userdata, nil
}

// GetUser fetches the user information from the Datastore.  It should
// fail with an error if the user/password is invalid, or if the user
// data was corrupted, or if the user can't be found.
func GetUser(username string, password string) (userdataptr *User, err error) {
	_, _, ok := fetchPublicKeys(username)
	if !ok {
		return nil, errors.New("INVALID CREDENTIALS (public keys not found in keystore)")
	}
	var user User
	if err = user.read(username, password); err != nil {
		return
	}
	return &user, nil
}

func (userdata *User) newFile(filename string, data []byte) {
	shareaddr := randomUUID()
	fileheadaddr := randomUUID()
	contentaddr := randomUUID()
	nextcontentaddr := randomUUID()

	masterkey := userlib.RandomBytes(16)

	var sharehead ShareNode
	var da DocAddr = DocAddr{Author: userdata.Username, Addr: shareaddr}
	sharehead.Meta = ShareNodeMeta{DocAddr: da, Parent: DocAddr{Addr: fileheadaddr, Author: userdata.Username}, Children: []DocAddr{}, Depth: 0}
	sharehead.Body = ShareNodeBody{LastModified: da, Data: []ShareEntry{ShareEntry{PubKey: userdata.PKEEncKey, Value: pkEnc(userdata.PKEEncKey, masterkey, "masterkey1")}}}
	sharehead.Doc.Meta = jsonMarshal(sharehead.Meta)
	sharehead.Doc.Body = jsonMarshal(sharehead.Body)
	sharehead.Doc.signMeta(userdata.DSSignKey)
	sharehead.Doc.signBody(userdata.DSSignKey)
	sharehead.Doc.write(shareaddr)

	var filehead EncryptedDocument
	filehead.Body = jsonMarshal(FileHeadBody{Next: contentaddr, Last: nextcontentaddr, Pages: 1})
	filehead.write(fileheadaddr, hashKDFnum(masterkey, 0))

	var content EncryptedDocument
	content.Body = jsonMarshal(FileNodeBody{Next: nextcontentaddr, Data: data})
	content.write(contentaddr, hashKDFnum(masterkey, 1))

	userdata.Files = append(userdata.Files, FileHandle{FilenameHash: userlib.Hash([]byte(filename)), ShareNode: DocAddr{Addr: shareaddr, Author: userdata.Username}})
	userdata.write()
}

// StoreFile stores a file in the datastore.
// The plaintext of the filename + the plaintext and length of the filename
// should NOT be revealed to the datastore!
func (userdata *User) StoreFile(filename string, data []byte) {
	addr, _, err := userdata.lookup(filename)
	if err == nil {
		addr, symkey, err := acquire(addr, userdata.PKEEncKey, userdata.PKEDecKey)
		if err != nil {
			userlib.DebugMsg("WARNING: file corrupt, write skipped")
			return
		}
		var filehead EncryptedDocument
		nextaddr := randomUUID()
		filehead.Body = jsonMarshal(FileHeadBody{Next: nextaddr, Last: nextaddr, Pages: 0})
		filehead.write(addr.Addr, hashKDFnum(symkey, 0))
		userdata.AppendFile(filename, data)
	} else {
		userdata.newFile(filename, data)
	}
	userdata.write()
}

func keyqual(k1 userlib.PKEEncKey, k2 userlib.PKEEncKey) bool {
	return k1.PubKey.N.Cmp(k2.PubKey.N) == 0 && k1.PubKey.E == k2.PubKey.E
}

func acquire(sharenode DocAddr, pubkey userlib.PKEEncKey, privkey userlib.PKEDecKey) (addr DocAddr, symkey []byte, err error) {
	var s ShareNode
	if err = s.read(sharenode); err != nil {
		return
	}
	if s.Meta.Depth == 0 {
		addr = s.Meta.Parent
		symkey, err = s.getSymKey(pubkey, privkey)
		return
	}
	var nextkey userlib.PKEDecKey
	if nextkey, err = s.getPrivKey(pubkey, privkey); err != nil {
		return
	}
	addr, symkey, err = acquire(s.Meta.Parent, s.Body.PubKey, nextkey)
	return
}

func (userdata *User) lookup(filename string) (addr DocAddr, f *FileHandle, err error) {
	for i, e := range userdata.Files {
		if userlib.Hash([]byte(filename)) == e.FilenameHash {
			return e.ShareNode, &(userdata.Files[i]), nil
		}
	}
	err = errors.New("FILE NOT FOUND")
	return
}

// AppendFile adds on to an existing file.
// Append should be efficient, you shouldn't rewrite or reencrypt the
// existing file, but only whatever additional information and
// metadata you need.
func (userdata *User) AppendFile(filename string, data []byte) (err error) {
	if userdata.update() != nil {
		return errors.New("user struct compromised")
	}
	start, _, er := userdata.lookup(filename)
	if er != nil {
		err = er
		return
	}
	addr, symkey, er := acquire(start, userdata.PKEEncKey, userdata.PKEDecKey)
	if er != nil {
		err = er
		return
	}
	var filehead EncryptedDocument
	if err = filehead.read(addr.Addr, hashKDFnum(symkey, 0)); err != nil {
		return
	}

	var fhb FileHeadBody
	if err = jsonUnmarshal(filehead.Body, &fhb); err != nil {
		return
	}

	var content EncryptedDocument
	nextcontentaddr := randomUUID()
	content.Body = jsonMarshal(FileNodeBody{Next: nextcontentaddr, Data: data})
	content.write(fhb.Last, hashKDFnum(symkey, fhb.Pages+1))
	fhb.Pages++
	fhb.Last = nextcontentaddr
	filehead.Body = jsonMarshal(fhb)
	filehead.write(addr.Addr, hashKDFnum(symkey, 0))

	return nil
}

// This loads a file from the Datastore.
//
// It should give an error if the file is corrupted in any way.
func (userdata *User) LoadFile(filename string) (data []byte, err error) {
	if userdata.update() != nil {
		return nil, errors.New("user struct compromised")
	}
	start, _, er := userdata.lookup(filename)
	if er != nil {
		err = er
		return
	}
	addr, symkey, er := acquire(start, userdata.PKEEncKey, userdata.PKEDecKey)
	if er != nil {
		err = er
		return
	}

	var filehead EncryptedDocument
	if err = filehead.read(addr.Addr, hashKDFnum(symkey, 0)); err != nil {
		return
	}
	var fhb FileHeadBody
	if err = jsonUnmarshal(filehead.Body, &fhb); err != nil {
		return
	}

	var curr = fhb.Next
	for i := 1; i <= fhb.Pages; i++ {
		var doc EncryptedDocument
		if err = doc.read(curr, hashKDFnum(symkey, i)); err != nil {
			return
		}
		var body FileNodeBody
		if err = jsonUnmarshal(doc.Body, &body); err != nil {
			return
		}
		data = append(data, body.Data...)
		curr = body.Next
	}
	return
}

type ShareToken struct {
	Data []byte
	Sig  []byte
}

func initializeNewShareChild(parent ShareNode, owner string) (share ShareNode, err error) {
	rpk, _, ok := fetchPublicKeys(owner)
	if !ok {
		err = errors.New("RECIPIENT NOT FOUND")
		return
	}
	ek, dk, err := userlib.PKEKeyGen()
	if err != nil {
		panic("PKEKeyGen failed unexpectedly!!")
	}

	symkey := userlib.RandomBytes(16)

	addr := randomUUID()

	share.Meta.DocAddr.Author = owner
	share.Meta.DocAddr.Addr = addr
	share.Meta.Parent = parent.Meta.DocAddr
	share.Meta.Depth = parent.Meta.Depth + 1
	share.Body.PubKey = ek
	share.Body.EncryptedPrivKey = symEnc(symkey, jsonMarshal(dk))
	share.Body.Data = []ShareEntry{ShareEntry{PubKey: rpk, Value: pkEnc(rpk, symkey, "symkey1")}}
	return share, nil
}

// ShareFile creates a sharing record, which is a key pointing to something
// in the datastore to share with the recipient.
// This enables the recipient to access the encrypted file as well
// for reading/appending.
// Note that neither the recipient NOR the datastore should gain any
// information about what the sender calls the file.  Only the
// recipient can access the sharing record, and only the recipient
// should be able to know the sender.
func (userdata *User) ShareFile(filename string, recipient string) (magicString string, err error) {
	if userdata.update() != nil {
		return "", errors.New("user struct compromised")
	}
	start, _, er := userdata.lookup(filename)
	if er != nil {
		err = er
		return
	}
	_, _, err = acquire(start, userdata.PKEEncKey, userdata.PKEDecKey)
	if err != nil {
		return
	}
	var startNode ShareNode
	if err = startNode.read(start); err != nil {
		return
	}

	startsym, er := startNode.getSymKey(userdata.PKEEncKey, userdata.PKEDecKey)
	if er != nil {
		err = er
		return
	}

	share, err := initializeNewShareChild(startNode, recipient)
	if err != nil {
		return "", err
	}
	share.Body.LastModified = start
	share.Doc.Meta = jsonMarshal(share.Meta)
	share.Doc.Body = jsonMarshal(share.Body)
	share.Doc.signMeta(userdata.DSSignKey)
	share.Doc.signBody(userdata.DSSignKey)
	share.Doc.write(share.Meta.DocAddr.Addr)

	startNode.Body.LastModified = start
	startNode.Meta.Children = append(startNode.Meta.Children, share.Meta.DocAddr)
	startNode.Body.Data = append(startNode.Body.Data, ShareEntry{PubKey: share.Body.PubKey, Value: pkEnc(share.Body.PubKey, startsym, "startsym1")})
	startNode.Doc.Meta = jsonMarshal(startNode.Meta)
	startNode.Doc.Body = jsonMarshal(startNode.Body)
	startNode.Doc.signMeta(userdata.DSSignKey)
	startNode.Doc.signBody(userdata.DSSignKey)
	startNode.Doc.write(start.Addr)

	startNode.read(start)

	rpk, _, ok := fetchPublicKeys(recipient)
	if !ok {
		err = errors.New("RECIPIENT NOT FOUND")
		return
	}
	enc := pkEnc(rpk, jsonMarshal(share.Meta.DocAddr.Addr), "shareMetaDocAddr")
	return string(jsonMarshal(ShareToken{Data: enc, Sig: DSSign(userdata.DSSignKey, enc)})), nil
}

// ReceiveFile ...
// Note recipient's filename can be different from the sender's filename.
// The recipient should not be able to discover the sender's view on
// what the filename even is!  However, the recipient must ensure that
// it is authentically from the sender.
func (userdata *User) ReceiveFile(filename string, sender string, magic string) error {
	if userdata.update() != nil {
		return errors.New("user struct compromised")
	}
	fHash := userlib.Hash([]byte(filename))
	for _, fname := range userdata.Files {
		if fname.FilenameHash == fHash {
			return errors.New("filename in use")
		}
	}

	_, svk, ok := fetchPublicKeys(sender)
	if !ok {
		return errors.New("SENDER NOT FOUND")
	}

	var token ShareToken
	if err := jsonUnmarshal([]byte(magic), &token); err != nil {
		return errors.New("token unmarshal failed")
	}
	addr, err := pkDec(userdata.PKEDecKey, token.Data)
	var docaddr DocAddr
	docaddr.Author = userdata.Username
	if err := jsonUnmarshal(addr, &docaddr.Addr); err != nil {
		return errors.New("token uuid unmarshal failed")
	}
	if err != nil {
		return err
	}
	if err := userlib.DSVerify(svk, token.Data, token.Sig); err != nil {
		return errors.New("integrity failed")
	}
	_, _, err = acquire(docaddr, userdata.PKEEncKey, userdata.PKEDecKey)
	if err != nil {
		return err
	}
	var node ShareNode
	err = node.read(docaddr)
	if err != nil {
		return err
	}
	node.Body.LastModified = node.Meta.DocAddr
	node.Doc.Meta = jsonMarshal(node.Meta)
	node.Doc.Body = jsonMarshal(node.Body)
	node.Doc.signMeta(userdata.DSSignKey)
	node.Doc.signBody(userdata.DSSignKey)
	node.Doc.write(node.Meta.DocAddr.Addr)

	userdata.Files = append(userdata.Files, FileHandle{FilenameHash: fHash, ShareNode: docaddr})
	userdata.write()
	return nil
}

//changes the key of this node. caller must also update keys upstream or else integrity is violated.
func (s *ShareNode) changeKeys(ownPubKey userlib.DSVerifyKey) (symkey []byte) {
	rpk, _, ok := fetchPublicKeys(s.Meta.DocAddr.Author)
	if !ok {
		userlib.DebugMsg("WARNING: author for this sharenode doesnt seem to exist")
	}

	ek, dk, err := userlib.PKEKeyGen()
	if err != nil {
		panic("PKEKeyGen failed unexpectedly!!")
	}
	symkey = userlib.RandomBytes(16)
	s.Body.PubKey = ek
	s.Body.EncryptedPrivKey = symEnc(symkey, jsonMarshal(dk))
	s.Body.Data = []ShareEntry{ShareEntry{PubKey: rpk, Value: pkEnc(rpk, symkey, "symkey2")}}
	for _, entry := range s.Meta.Children {
		var child ShareNode
		if err := child.read(entry); err != nil {
			userlib.DebugMsg("WARNING: child sharenode is corrupted")
			continue
		}
		if err := child.verifySignatures(ownPubKey); err != nil {
			userlib.DebugMsg("WARNING: child sharenode is corrupted")
			continue
		}
		s.Body.Data = append(s.Body.Data, ShareEntry{PubKey: child.Body.PubKey, Value: pkEnc(child.Body.PubKey, symkey, "symkey3")})
	}
	return symkey
}

func (s *ShareNode) dropChild(username string) error {
	dropped := false
	for i, c := range s.Meta.Children {
		if c.Author == username {
			s.Meta.Children[i] = s.Meta.Children[len(s.Meta.Children)-1]
			s.Meta.Children = s.Meta.Children[:len(s.Meta.Children)-1]
			dropped = true
			break
		}
	}
	if !dropped {
		return errors.New("no such child")
	}
	return nil
}

// RevokeFile removes target user's access.
func (userdata *User) RevokeFile(filename string, target string) (err error) {
	f, _, err := userdata.lookup(filename)
	if err != nil {
		return err
	}
	addr, oldsymkey, err := acquire(f, userdata.PKEEncKey, userdata.PKEDecKey)
	if err != nil {
		return err
	}

	var s ShareNode
	if err := s.read(f); err != nil {
		return err
	}

	err = s.dropChild(target)
	if err != nil {
		return err
	}

	newsymkey := s.changeKeys(userdata.DSVerifyKey)
	s.Body.LastModified = f
	s.Doc.Meta = jsonMarshal(s.Meta)
	s.Doc.signMeta(userdata.DSSignKey)
	s.Doc.Body = jsonMarshal(s.Body)
	s.Doc.signBody(userdata.DSSignKey)
	s.Doc.write(s.Meta.DocAddr.Addr)
	for s.Meta.Depth > 0 {
		if err = s.readAndVerifyParent(s); err != nil {
			return err
		}
		newsymkey = s.changeKeys(userdata.DSVerifyKey)
		s.Body.LastModified = f
		s.Doc.Body = jsonMarshal(s.Body)
		s.Doc.signBody(userdata.DSSignKey)
		s.Doc.write(s.Meta.DocAddr.Addr)
	}

	var filehead EncryptedDocument
	if err = filehead.read(addr.Addr, hashKDFnum(oldsymkey, 0)); err != nil {
		return
	}
	var fhb FileHeadBody
	if err = jsonUnmarshal(filehead.Body, &fhb); err != nil {
		return
	}
	filehead.write(addr.Addr, hashKDFnum(newsymkey, 0))

	var curr = fhb.Next
	for i := 1; i <= fhb.Pages; i++ {
		var doc EncryptedDocument
		if err = doc.read(curr, hashKDFnum(oldsymkey, i)); err != nil {
			return
		}
		var body FileNodeBody
		if err = jsonUnmarshal(doc.Body, &body); err != nil {
			return
		}
		doc.write(curr, hashKDFnum(newsymkey, i))
		curr = body.Next
	}
	return
}
