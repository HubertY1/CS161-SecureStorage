# CS161 Project 2 Design Document
Hubert Yuan

## Users

A User is a `(username, password)` pair.

User Alice stores public encryption and signature keys at address `"e Alice"` and `"v Alice"` in the Keystore.

A username is registered if their public key exists in the keystore.

Users generate a (symmetric) master key by calling `ArgonPKDF(password, username, 16)`. User data on the Datastore is encrypted with the master key.

User data consists of an array of `(Hash(filename), DocAddr)` triples, where `DocAddr` points to an owned Sharing Node of some file.

User data is stored in the body of a Datastore Document at UUID `Hash(username)`.


## Datastore Document
A Datastore Document Address `(DocAddr)` is a pair `(Addr: UUID, Author: string)`.
A Datastore Document is a JSON object with fields of byte array type.
There are two types of Datastore Documents:

	SecureDocument {
		Meta:     JSON serialization of a metadata object. 
		MetaSig:  RSA signature of the Meta field.
		Body:     JSON serialization of the main content.
		BodySig:  RSA signature of the Body field.
	}	

	EncryptedDocument {
		Body:     Encrypted JSON serialization of the main content.
		BodyHMAC: HMAC Signature of the main content.
	}

Each EncryptedDocument is associated with a single symmetric key, but HashKDF is used to derive distinct keys for encryption and integrity.

## Files
A file is a file head and one or more file nodes. A file is encrypted using a symmetric key. This key can change at any time, but is tracked by a sharing head. If access permissions to the file are to be revoked for some user, the entire file must be reencrypted with a new randomly generated symmetric key, and the new key propagated to the sharing head.
##### 1. File Storage
A file head is a Encrypted Document. It contains:
- **N**, the number of file nodes
- **Next**, the UUID of the next file node
- **Last**, the UUID of the *reserved space* following the final file node.

All UUIDs are randomly generated.

A file node is a Encrypted Document that contains some Document data and the UUID of the next file node. The symmetric key used to encrypt the file node is `HashKDF(filehead.symkey, nodeid)` where `nodeid` starts at 1 for the first node and increments each node. The file head itself is considered node 0.

To read a file, a user walks down the file nodes, decrypting each one. The file is all of their data appended together.

To replace a file, a user creates a new file node containing the desired data, sets the node count of the file head to 1,  and updates it to point to the new node.
##### 4. File Append
To append to a file, a user creates a new file node containing the desired data, increments the node count of the file head, and attaches the new node to the last file node. When a user instantiates a file node, the address of the next file node is reserved in advance and written to the Last field of the file head. The user never has to read any file nodes.

The author of file nodes or even the file head are not particularly important. The effective owner of the file is the owner of the sharing head.

## Sharing tree
A sharing tree provides the symmetric key of a file to users in a hierarchical fashion. Each sharing node has its own RSA private and public keypairs, which can change at any time. Each sharing node also has a secret symmetric key, which it exposes by encrypting it with the public keys of its child nodes as well as the personal public key of the owner of the node. The first sharing node is called the sharing head and its symmetric key is the one used to encrypt the document.

Anyone who knows the private key of a sharing node *controls* the node. The original author of a sharing node *owns* the node. The only way to reliably control a node is to own the node or one of its descendants. The structure of a sharing node follows:

	ShareNode {
		Meta: {
			DocAddr:           The (uuid, author) pair for this ShareNode
			Parent:            DocAddr
			Children:          []DocAddr
		}
		Body: {
			PubKey             RSAPubKey
			EncryptedPrivKey   RSAPubKey encrypted by the SymKey
			LastModified:      The DocAddr of the sharing node owned by whoever signed the Body last
			Data:              (RSAPubKey, EncryptedSymKey) pairs
	}	

The Children field in the metadata authoritatively lists all legitimate children of the sharing node. Whoever writes to a sharing node has the responsibility of properly exposing the necessary information to all children. Also, the user must sign either of the two Meta/Body fields of the SecureDocument with his own public key. Signatures should only be accepted for the Meta field only if they are from the original author or the owner of the direct parent, and for the Body field only if they own a direct descendant, the veracity of which can be confirmed by walking up the tree from the LastModified field.

In order to read a file from a valid sharing node, a user walks up the sharing tree until he reaches the sharing head. The sharing head exposes the symmetric key of the file head instead of its own private key. The parent of the sharing head is also the file head.
##### 2. File Sharing
In order to grant file access to a user, the owner of a sharing node creates a new child sharing node with the recipient listed as author. The UUID of the new sharing node is given to the file recipient. The file recipient can then asynchronously claim the node. When generating the magic string, the UUID must be encrypted with the recipient's private key, and signed with the author's private key to ensure confidentiality and integrity.
##### 4. File Revocation
In order to revoke file access from a user, the owner of a sharing node drops the associated sharing node as a child, and stops publishing key updates to the former child public key. The owner must then walk up the sharing tree and change all three keys for each sharing node until they reach the sharing head. They must then use the new symmetric key of the sharing head to reencrypt the entire document.

Whenever a user encrypts anything with a foreign public key, they must check the signatures of the Secure Document to ensure that it was written by an authorized source. If keys are modified by illegitimate users or the signatures otherwise cannot be verified, the sharing node is considered corrupt and should be dropped.

## Security Analysis

- An attacker may attempt to forge a file's contents by overwriting the Body of its sharing head to expose a new symmetric key and encrypting forged file contents with the new key. This fails because the attacker is unable to sign the Body with an appropriate LastModified node, as he does not control any sharing nodes. Any user reading the altered sharing head will therefore reject it. Since the legitimacy of the keys exposed by the sharing node is backed by the sharing structure in the Meta fields, it is impossible to forge any keys by changing the Body field only. 

- An attacker may attempt to gain access to the file by changing some or all of the Meta fields of the sharing nodes in order to modify the structure of the sharing tree. This fails by an inductive argument. Any user who accesses a sharing node either owns it or does so through a child node's Parent DocAddr, which includes the expected author. In either case, the author of the node is known, so a forged Meta will be rejected because the attacker cannot sign it with the original author's private key. This implies that the attacker must change the Meta fields of the child nodes as well, and set himself as the expected author. But this incurs the same problem one level lower, so by induction the attacker will end up corrupting every sharing node, and since every user of the file owns a sharing node, every user of the file will recognize a breach of integrity.

- An attacker may attempt to modify a file by removing the middle parts of a its content originating from appends by removing file nodes. This fails for two reasons. First, the file head lists the expected number of pages of the file, so removing nodes will cause the last nodes to fail to parse, which implies the file is corrupt. Nodes cannot be arbitrarily reordered because each points to the address of the next one. Second, the file nodes do not all use the same encryption key; keys are instead derived from the master symmetric key tracked by the sharing head using HashKDF and the page number. So any manipulation that changes the page number of a file node will invalidate its symmetric key, causing the HMAC verification to fail.

- An attacker who currently has access to a file may attempt to avoid file revocation by replacing the public key of some file node with a compromised one, in the hope that future secret keys will be encrypted with that public key, allowing the attacker to walk the sharing tree and read the file even if his file node is revoked. This fails because the attacker is only allowed to change the keys of direct ancestor nodes. Attempts to change the keys of sibling or descendant nodes will cause an integrity violation when the signature is read. If the parent of the attacker's node revokes the attacker's file permissions, every ancestor's keys will be replaced by random ones as part of the revocation procedure. So a owner of a file node cannot perform any manipulations that will not be undone by revocation.

